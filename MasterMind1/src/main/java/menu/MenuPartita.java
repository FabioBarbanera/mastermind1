package menu;

import java.time.Instant;
import java.util.Scanner;
import exception.NumeroColoriException;
import exception.NumeroPosizioniException;
import exception.NumeroTentativiException;
import partita.Partita;

public class MenuPartita {

	public static void avvia(Scanner scanner, Instant start) {

		int sceltaNumeroTentativi = 0;
		int sceltaNumeroColori = 0;
		int sceltaNumeroPosizioni = 0;

		System.out.println("Partita numero " + Partita.contatorePartita);
		System.out.println(" ");
		do {
			try {
				System.out.println("Inserisci il numero di tentativi (1-12)");
				String stringSceltaTentativi = scanner.nextLine().trim().replaceAll("\\s", "");
				/*
				 * Metto questa condizione di verifica perché nel parseInt lo zero prima all'inizio viene tolto dal metodo
				 */
				if(stringSceltaTentativi.contains("0")) {
					System.err.println(NumeroTentativiException.numeroTentativoNonValido());
				} else {
					sceltaNumeroTentativi = Integer.parseInt(stringSceltaTentativi);
				}
			}catch(NumberFormatException ex) {
				System.err.println(NumeroTentativiException.numeroTentativoNonValido());
			}
		} while (sceltaNumeroTentativi < 1 || sceltaNumeroTentativi > 12);

		do {
			try {
				System.out.println("Inserisci il numero di colori (2-6)");
				String stringSceltaNumeroColori = scanner.nextLine().trim().replaceAll("\\s", "");
				if(stringSceltaNumeroColori.contains("0")) {
					System.err.println(NumeroColoriException.numeroColoriNonValido());
				}else {
					sceltaNumeroColori = Integer.parseInt(stringSceltaNumeroColori);
				}
			} catch (NumberFormatException ex) {
				System.err.println(NumeroColoriException.numeroColoriNonValido());
			}
		} while (sceltaNumeroColori < 2 || sceltaNumeroColori > 6);

		do {
			try {
				System.out.println("Inserisci il numero di posizioni (3-6)");
				String stringNumeroPosizioni = scanner.nextLine().trim().replaceAll("\\s", "");
				if(stringNumeroPosizioni.contains("0")){
					System.err.println(NumeroPosizioniException.numeroPosizioniNonValido());	
				}else{
					sceltaNumeroPosizioni = Integer.parseInt(stringNumeroPosizioni);
				}
			} catch (NumberFormatException ex) {
				System.err.println(NumeroPosizioniException.numeroPosizioniNonValido());
			}
		} while (sceltaNumeroPosizioni < 3 || sceltaNumeroPosizioni > 6);

		System.out.println("Numero tentativi:" + sceltaNumeroTentativi);
		System.out.println("Numero colori:" + sceltaNumeroColori);
		System.out.println("Numero posizioni:" + sceltaNumeroPosizioni);
		System.out.println("");

		Partita partita = new Partita(sceltaNumeroTentativi, sceltaNumeroColori, sceltaNumeroPosizioni);
		partita.giocaPartita(scanner, start);

	}

}
