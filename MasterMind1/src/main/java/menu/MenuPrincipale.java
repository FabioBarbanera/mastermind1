package menu;

import java.time.Instant;
import java.util.Scanner;

import exception.ScannerException;
import partita.Partita;
import partita.Tentativo;

public class MenuPrincipale {

	private MenuPrincipale() {
	}

	public static void avviaMenu(Scanner scanner) {

		if (scanner == null) {
			System.err.println(ScannerException.ScannerNull());
			scanner = new Scanner(System.in);
			System.err.println("Scanner Inizializzato! Puoi proseguire con il gioco!");
		}

		boolean interruttore = true;

		while (interruttore) {
			System.out.println("Digita H per Help");
			System.out.println("Digita A per avvio partita");
			System.out.println("Digita Q per uscire");
			String scelta = scanner.nextLine().trim().toUpperCase().replaceAll("\\s", "");

			switch (scelta) {
			case "H":
				System.out.println(
						"MasterMiand è un gioco di crittoanalisi in cui un giocatore, il \"decodificatore\",\n"
						+ "deve indovinare il codice segreto composto dal suo avversario, detto \"codificatore\"");
				System.out.println("Per maggiori informazioni visita la pagina: https://it.wikipedia.org/wiki/Mastermind");
				System.out.println(" ");
				break;
			case "A":
				System.out.println("BUONA PARTITA!");
				Instant start = Instant.now();
				MenuPartita.avvia(scanner, start);
				do {
					System.out.println("Vuoi Giocare ancora? Digita \"SI\" o \"NO\"");
					String sceltaUtenteSeRigiocare = scanner.nextLine().trim().toUpperCase().replaceAll("\\s", "");

					if (sceltaUtenteSeRigiocare.equals("SI")) {
						Tentativo.contatoreTentativi = 1;
						MenuPrincipale.avviaMenu(scanner);
						interruttore = false;

					} else if (sceltaUtenteSeRigiocare.equals("NO")) {
						System.out.println("Arrivederci, grazie per aver giocato con noi!");
						interruttore = false;
					}
				} while (interruttore);
				break;
			case "Q":
				System.out.println("Arrivederci, grazie per aver giocato con noi!");
				Partita.contatorePartita = 1;
				interruttore = false;
				break;
			default:
				System.err.println("Scelta non disponibile");
				break;
			}
		}
	}
}
