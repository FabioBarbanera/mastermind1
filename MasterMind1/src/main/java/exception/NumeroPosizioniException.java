package exception;

public class NumeroPosizioniException extends Exception{

	private NumeroPosizioniException(String message) {
		super(message);
	}
	
	public static NumeroPosizioniException numeroPosizioniNonValido() {
		return new NumeroPosizioniException("Numero di posizioni non valido");
	}
	
}
