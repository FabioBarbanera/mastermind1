package exception;

public class CodiceDaConfrontareException extends Exception{

	private CodiceDaConfrontareException(String message) {
		super(message);
	}
	
	public static CodiceDaConfrontareException codiceNullo() {
		return new CodiceDaConfrontareException("Codice da confrontare nullo");
	}
}
