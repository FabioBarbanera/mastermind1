package exception;

public class NumeroColoriException extends Exception{
	
	private NumeroColoriException(String message) {
		super(message);
	}
	
	public static NumeroColoriException numeroColoriNonValido() {
		return new NumeroColoriException("Numero di colori non valido");
	}
	
}
