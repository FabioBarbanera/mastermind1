package exception;

public class NumeroTentativiException extends Exception{

	private NumeroTentativiException(String message) {
		super(message);
	}
	
	public static NumeroTentativiException numeroTentativoNonValido() {
		return new NumeroTentativiException("Numero di tentativi scelti non valido");
	}
}
