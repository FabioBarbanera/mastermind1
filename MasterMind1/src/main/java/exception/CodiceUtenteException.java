package exception;

public class CodiceUtenteException extends Exception {

	private CodiceUtenteException(String message) {
		super(message);
	}
	
	public static CodiceUtenteException formatoNonValido() {
		return new CodiceUtenteException("Formato non valido");
	}
	
	public static CodiceUtenteException numeroNonValido() {
		return new CodiceUtenteException("Numero non valido");
	}
}
