package exception;

public class ScannerException extends Exception{

	private ScannerException(String message) {
		super(message);
	}
	
	public static ScannerException ScannerNull() {
		return new ScannerException("Lo scanner è nullo");
	}
}
