package codici;

public class CodiceUtente {

	private Integer[] codiceUtente;
	
	private CodiceUtente(Integer[] codiceUtente) {
		this.codiceUtente = codiceUtente;
	}
	
	public static CodiceUtente creaCodiceUtenteInteger(String stringaCodiceUtente) throws NullPointerException{

		String[] arrayCodiceStringSplittato = stringaCodiceUtente.split(",");
		Integer[]arrayCodiceTrasformatoInInteger = new Integer[arrayCodiceStringSplittato.length];
		
		for(int i=0; i<arrayCodiceStringSplittato.length; i++) {
			arrayCodiceTrasformatoInInteger[i]=Integer.parseInt(arrayCodiceStringSplittato[i]);
		}
		
		return new CodiceUtente(arrayCodiceTrasformatoInInteger);
	}

	public Integer[] getCodiceUtente() {
		return codiceUtente;
	}
	
}
