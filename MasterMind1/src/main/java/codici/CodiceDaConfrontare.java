package codici;

import java.util.Arrays;
import java.util.Random;

public class CodiceDaConfrontare {

	private Integer[]codiceDaConfrontare;
	
	private CodiceDaConfrontare(Integer[]codiceDaConfrontare) {
		this.codiceDaConfrontare = codiceDaConfrontare;
	}

	/**
	 * Metodo per creare Array contenente il codice vincente random
	 * @param numeroPosizioni
	 * @param numeroColori
	 * @return
	 */
	public static CodiceDaConfrontare generaCodicdeDaConfrontare(int numeroPosizioni, int numeroColori) {

		Integer[] codiceDaConfrontare = new Integer[numeroPosizioni];
		for (int i = 0; i < numeroPosizioni; i++) {
			Random random = new Random();
			int numeroCodiceVincente = random.nextInt((numeroColori  - 1) + 1) +1;
			codiceDaConfrontare[i] = numeroCodiceVincente;
		}
		return new CodiceDaConfrontare(codiceDaConfrontare);
	}

	public Integer[] getCodiceDaConfrontare() {
		return codiceDaConfrontare;
	}

	@Override
	public String toString() {
		return  Arrays.toString(codiceDaConfrontare);
	}
}
