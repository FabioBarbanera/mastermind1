package partita.esito;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import partita.Partita;
import partita.Tentativo;

public class EsitoPartita {

	private EsitoTentativo esitoTentativo;

	public EsitoPartita(EsitoTentativo esitoTentativo) {
		this.esitoTentativo = esitoTentativo;
	}

	public boolean controllaEsito(int numeroTentativi, boolean interruttore, Instant start) {

		/*
		 * Se è vero che gli array sono uguali(quindi che l'utente ha inserito la
		 * sequenza giusta gli dico che ha vinto ed esco dal ciclo, mettendo
		 * l'interruttore a true Termino il il conto della durata della partita e la
		 * stampo a video
		 */
		if (confrontaUguaglianzaArray(esitoTentativo.getTentativo().getCodicedaConfrontare().getCodiceDaConfrontare(),
				esitoTentativo.getTentativo().getCodiceUtente().getCodiceUtente()) == true) {
			System.out.println("COMPLIMENTI! Hai indovinato il codice segreto: "
					+ (esitoTentativo.getTentativo().getCodicedaConfrontare().toString()));
			Instant endPartita = Instant.now();
			Duration tempoPartita = Duration.between(start, endPartita);
			System.out.println("La partita " + (Partita.contatorePartita - 1) + " è durata: "
					+ tempoPartita.getSeconds() + " secondi");
			return true;
		}

		/*
		 * Se invece l'utente ha finito i tentativi (quantità scelta da lui
		 * precedentemente), gli dico che non ha indovinato il codice ed esco dal ciclo,
		 * mettendo l'interruttore a true Termino il il conto della durata della partita
		 * e la stampo a video
		 */
		if (Tentativo.getContatoreTentativi() > numeroTentativi && confrontaUguaglianzaArray(
				esitoTentativo.getTentativo().getCodicedaConfrontare().getCodiceDaConfrontare(),
				esitoTentativo.getTentativo().getCodiceUtente().getCodiceUtente()) == false) {
			System.out.println("Mi dispiace, non sei riuscito ad indovinare! il codice segreto era: "
					+ (esitoTentativo.getTentativo().getCodicedaConfrontare().toString()));
			Instant endPartita = Instant.now();
			Duration tempoPartita = Duration.between(start, endPartita);
			System.out.println("La partita " + (Partita.contatorePartita - 1) + " è durata: "
					+ tempoPartita.getSeconds() + " secondi");
			return true;
		}
		return interruttore;

	}

	public boolean confrontaUguaglianzaArray(Integer[] codiceDaConfrontare, Integer[] codiceUtente) {

		if (Arrays.equals(codiceDaConfrontare, codiceUtente)) {
			return true;
		} else {
			return false;
		}
	}
}