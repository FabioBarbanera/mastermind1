package partita.esito;

import partita.Tentativo;

public class EsitoTentativo {

	Tentativo tentativo;
	final Character COLORE_E_POSIZIONE_GIUSTO = '+';
	final Character SOLO_COLORE_GIUSTO = '-';

	public EsitoTentativo(Tentativo tentativo) {
		this.tentativo = tentativo;
	}

	public void controllaEsito(int numeroPosizioni) {

		int contatoreNumeriPosizioniEColoriGiusti = 0;
		Integer[] codiceDaConfrontare = this.tentativo.getCodicedaConfrontare().getCodiceDaConfrontare();
		Integer[] codiceUtente = this.tentativo.getCodiceUtente().getCodiceUtente();
		Integer[] arrayCodiceDaConfrontarePerIlConfronto = new Integer[codiceDaConfrontare.length];
		Integer[] arrayCodiceUtentePerIlConfronto = new Integer[codiceUtente.length];
		Character[] arrayRisultato = new Character[numeroPosizioni];

		/*
		 * Mi preparo altri due array, uno doppione del codice da confrontare e uno doppione del codice utente 
		 * perche li userò per il confontro e per verificare che ci sono numeri uguali nei due array
		 * li dovrò azzerare per non renderli disponibili al confronto del ciclo successivo
		 * quindi per non azzerare i codici ufficiali mi preparo dei cloni
		 */
		for (int i = 0; i < codiceDaConfrontare.length; i++) {
			arrayCodiceDaConfrontarePerIlConfronto[i] = codiceDaConfrontare[i];
			arrayCodiceUtentePerIlConfronto[i] = codiceUtente[i];

		}
		/*
		 * Con questo ciclo controllo i numeri alla stessa posizione dei due codici sono
		 * identici: Prima verifico se la combinazione è stata indovinata
		 * Se il numero del codice da confrontare alla stessa posizione del codice utente 
		 * sono uguali aggiungo all'array, che conterra i segni risultati dalla partita,
		 * aggiungo un +
		 */

		for (int i = 0; i < codiceDaConfrontare.length; i++) {
			if (arrayCodiceDaConfrontarePerIlConfronto[i] == arrayCodiceUtentePerIlConfronto[i]) {
				arrayCodiceDaConfrontarePerIlConfronto[i] = 0;
				arrayCodiceUtentePerIlConfronto[i] = 0;
				arrayRisultato[i] = this.COLORE_E_POSIZIONE_GIUSTO;
				contatoreNumeriPosizioniEColoriGiusti++;
			}
			/*
			 * E vedo con un contatore quanti colori e posizioni ci sono giusti
			 */
		}

		/*
		 * Se il contatore è lungo quanto il numero di posizione significa che 
		 * sicuramente sono stati indovinati tutti i numeri del codice 
		 * Quindi se questo è vero stampo il risultato
		 * 
		 */
		if(contatoreNumeriPosizioniEColoriGiusti == numeroPosizioni) {
			
			for(int i = 0; i < arrayRisultato.length; i++) {
				System.out.print(arrayRisultato[i]);
			}
		
		}else {
			/*
			 * Altrimenti verifico quali numeri giusti in posizioni diverse ci sono
			 * quando due numeri sono uguali li azzero ed aggiungo all'array di caratteri
			 * il carattere solo colore giusto
			 */
			for (int i = 0; i < codiceDaConfrontare.length; i++) {
				for (int j = 0; j < codiceUtente.length; j++) {
					if (arrayCodiceDaConfrontarePerIlConfronto[i] == arrayCodiceUtentePerIlConfronto[j]
							&& arrayCodiceDaConfrontarePerIlConfronto[i] != 0
							&& arrayCodiceUtentePerIlConfronto[j] != 0) {

						arrayRisultato[i] = this.SOLO_COLORE_GIUSTO;
						arrayCodiceDaConfrontarePerIlConfronto[i] = 0;
						arrayCodiceUtentePerIlConfronto[j] = 0;
					}
				}
			}
			
			/*
			 * Samtpo il risultato, prima i + e dopo i -
			 */
			
			for(int i=0; i<arrayRisultato.length;i++) {
				if (arrayRisultato[i] != null && arrayRisultato[i].equals(this.COLORE_E_POSIZIONE_GIUSTO)) {
					System.out.print(arrayRisultato[i]);
				}
			}
			
			for(int i=0; i<arrayRisultato.length;i++) {
				if(arrayRisultato[i] != null && arrayRisultato[i].equals(this.SOLO_COLORE_GIUSTO)) {
					System.out.print(arrayRisultato[i]);
				}
			}
		}
		System.out.println(" ");
	}

	public Tentativo getTentativo() {
		return tentativo;
	}
}
