package partita;

import codici.CodiceDaConfrontare;
import codici.CodiceUtente;

public class Tentativo {

	private CodiceDaConfrontare codicedaConfrontare;
	private CodiceUtente codiceUtente;
	public static Integer contatoreTentativi = 1;

	public Tentativo(CodiceDaConfrontare codiceDaConfrontare, CodiceUtente codiceUtente) {
		this.codicedaConfrontare=codiceDaConfrontare;
		this.codiceUtente=codiceUtente;
		contatoreTentativi++;
	}

	public CodiceDaConfrontare getCodicedaConfrontare() {
		return codicedaConfrontare;
	}

	public CodiceUtente getCodiceUtente() {
		return codiceUtente;
	}
	
	
	public static Integer getContatoreTentativi() {
		return contatoreTentativi;
	}

}
