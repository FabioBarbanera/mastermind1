package partita;

import java.time.Instant;
import java.util.Scanner;
import codici.CodiceDaConfrontare;
import codici.CodiceUtente;
import exception.CodiceDaConfrontareException;
import exception.CodiceUtenteException;
import partita.esito.EsitoPartita;
import partita.esito.EsitoTentativo;

public class Partita {

	private int numeroTentativi;
	private int numeroColori;
	private int numeroPosizioni;
	public static int contatorePartita = 1;
	
	public Partita(int numeroTentativi, int numeroColori, int numeroPosizioni) {
		this.numeroTentativi = numeroTentativi;
		this.numeroColori = numeroColori;
		this.numeroPosizioni = numeroPosizioni;
		contatorePartita++;
	}

	/**
	 * Metodo che permette di giocare la partita
	 * 
	 * @param scanner
	 */
	public void giocaPartita(Scanner scanner, Instant start) {

		/*
		 * Come prima cosa mi creo il codice vincente in maniera random
		 */
		CodiceDaConfrontare codiceDaConfrontare = CodiceDaConfrontare.generaCodicdeDaConfrontare(numeroPosizioni,
				numeroColori);

		/*
		 * Sollevo Eccezione nel caso in cuio fosse nullo
		 */
		if (codiceDaConfrontare == null) {
			System.err.println(CodiceDaConfrontareException.codiceNullo());
		}
//		String regexMatch = "[1-9][,]{"+ (numeroPosizioni-1) +"}[1-9]";
//		String regexMatch = "[1-9,{"+ (numeroPosizioni-1) +"}][0-9]";

		/*
		 * Stringa espressione regolare che permette di verificare condizioni di
		 * inserimento dell'utente
		 */
		String regexDaMatchare = "[1-9,?]+";
		CodiceUtente codiceUtente = null;
		
		/*
		 * Condizione per gestire l'uscita dal ciclo
		 */
		boolean interruttore = false;

		do {
			/*
			 * Inizializzo variabile booleana che sarà la condizione che mi permetterà di
			 * uscire dal secondo ciclo while
			 */
			boolean formatoStringaCorretto = false;
			do {
				/*
				 * Prendo la scelta dell'utente, diventerà il codiceUtente
				 */
				System.out.println("Tentativo " + Tentativo.getContatoreTentativi() + "/" + this.numeroTentativi
						+ ". Inserisci i " + this.numeroPosizioni + " colori da 1 a " + this.numeroColori
						+ " separati da virgola");
				String sceltaCodiceUtente = scanner.nextLine().trim().replaceAll("\\s", "");;

				/*
				 * Questa variabile mi servirà per valutare, attraverso la lunghezza della
				 * stringa inserita, se della scelta dell'utente è valida
				 */
				int lunghezzaControlloFormatoStringa = (this.numeroPosizioni * 2) - 1;

				/*
				 * Come prima cosa vedo se l'espressione regolare è rispettata dall'utente
				 */
				if (sceltaCodiceUtente.matches(regexDaMatchare) == false) {
					System.err.println(CodiceUtenteException.formatoNonValido());
					/*
					 * Poi appunto verifico se la lunghezza della stringa è corretta
					 */
				} else if (sceltaCodiceUtente.length() != lunghezzaControlloFormatoStringa) {
					System.err.println(CodiceUtenteException.formatoNonValido());
				} else {
					/*
					 * Se le condizione precedenti non sono si sono verificate allora posso creare il
					 * codice con i dati sicuramente giusti
					 */
					codiceUtente = CodiceUtente.creaCodiceUtenteInteger(sceltaCodiceUtente);

					/*
					 * Ora verifico se i numeri inseriti dall'utente sono al massimo quelli relativi(lo faccio con un metodo creato da me)
					 * alla scelta del menù precedente, posso farlo ora perche sono stati trasformati da String a Integer
					 */
					if (controllaValoriCodiceUtente(codiceUtente) == true) {
						/*
						 * Se effettivamente rispetta quel limite, allora esco dal ciclo
						 */
						formatoStringaCorretto = true;
					} else {
						/*
						 * Altrimenti ripeto l'inserimento perché non può essere inserito un numero di
						 * colore superiore alla scelta precedente
						 */
						System.err.println(CodiceUtenteException.numeroNonValido()+"! Come numero di colore puoi inserire massimo " + numeroColori + " ");
						formatoStringaCorretto = false;
					}
				}
			} while (formatoStringaCorretto == false); // cicla fino a che è false, quando è true non si verifica la condizione ed esce dal ciclo

			/*
			 * Mi creo un tentativo
			 */
			Tentativo tentativo = new Tentativo(codiceDaConfrontare, codiceUtente);
			/*
			 * Creo un esito del tentativo passandogli il tentativo
			 */
			EsitoTentativo esitoTentativo = new EsitoTentativo(tentativo);
			/*
			 * controllo l'esito grazie al tentativo che è stato passato
			 */
			esitoTentativo.controllaEsito(this.numeroPosizioni);

			/*
			 * Creo Esito partita e sarò l'esito del tentativo a determinare se la partita è vinta o meno
			 */
			EsitoPartita esitoPartita = new EsitoPartita(esitoTentativo);
			
			/*
			 * Controllo esito partita
			 */
			boolean risultatoPartita = esitoPartita.controllaEsito(numeroTentativi, interruttore, start);
			if(risultatoPartita==true) {
				interruttore=true;
			}

		} while (interruttore == false);
		/*
		 * Essendo uscito dal ciclo e non essendoci altre istruzioni all'interno del
		 * metodo Torno al menu precedente
		 */

	}

	private boolean controllaValoriCodiceUtente(CodiceUtente codiceUtente) {

		Integer[] codicePerControllo = new Integer[codiceUtente.getCodiceUtente().length];

		/*
		 * Non potendo ordinare l'array nato dalla scelta dell'utente me lo creo
		 * un'altro identico usando questo duplicato per la verifica, lasciando pulito
		 * quello ufficiale del codice
		 */
		for (int i = 0; i < codiceUtente.getCodiceUtente().length; i++) {
			codicePerControllo[i] = codiceUtente.getCodiceUtente()[i];
		}

		/*
		 * Per controllare se l'utente ha inserito numeri che sono al massimo il numero
		 * di colori che ha inserito precedentemente Mi ordino l'array in maniera
		 * decrescente
		 */
		for (int i = 0; i < codicePerControllo.length; i++) {
			for (int j = i + 1; j < codicePerControllo.length; j++) {
				if (codicePerControllo[i] < codicePerControllo[j]) {
					int temp = codicePerControllo[i];
					codicePerControllo[i] = codicePerControllo[j];
					codicePerControllo[j] = temp;
				}
			}
		}
		/*
		 * Se il primo elemento dell'array è un numero minore del numeroColori scelto
		 * dall'utente vuol dire che non ci sono numeri superiori al numero colori che
		 * l'utente ha scelto precedentemente quindi esco dal ciclo
		 */
		if (codicePerControllo[0] <= this.numeroColori) {
			return true;
		} else {
			return false;
		}
	}
}
